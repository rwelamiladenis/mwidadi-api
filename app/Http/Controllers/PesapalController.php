<?php

namespace App\Http\Controllers;

use Pesapal;
use Illuminate\Http\Request;

class PesapalController extends Controller
{
    public function confirmation($trackingid,$status,$payment_method,$merchant_reference)
    {
        return [
            'trackingId' => $trackingid,
            'status' => $status,
            'payment_method' => $payment_method,
            'merchant_reference' => $merchant_reference
        ];
        // $payments = Payments::where('tracking',$trackingid)->first();
        // $payments -> payment_status = $status;
        // $payments -> payment_method = $payment_method;
        // $payments -> save();
    }  
    public function payment(Request $request){//initiates payment
        // $payments = new Payment;
        // $payments -> businessid = Auth::guard('business')->id(); //Business ID
        // $payments -> transactionid = Pesapal::random_reference();
        // $payments -> status = 'NEW'; //if user gets to iframe then exits, i prefer to have that as a new/lost transaction, not pending
        // $payments -> amount = 10;
        // $payments -> save();

        $details = array(
            'amount' => $request->amount,
            'description' => $request->description,
            'type' => 'MERCHANT',
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phonenumber' => $request->phone,
            'reference' => Pesapal::random_reference()
            // 'height'=>'400px',
            //'currency' => 'USD'
        );
        $iframe=Pesapal::makePayment($details);
        return $iframe;

        // return view('payments.business.pesapal', compact('iframe'));
    }
    public function paymentsucces(Request $request)//just tells u payment has gone thru..but not confirmed
    {
        return $request;
        // $trackingid = $request->input('tracking_id');
        // $ref = $request->input('merchant_reference');

        // $payments = Payment::where('transactionid',$ref)->first();
        // $payments -> trackingid = $trackingid;
        // $payments -> status = 'PENDING';
        // $payments -> save();
        //go back home
        // $payments=Payment::all();
        // return view('payments.business.home', compact('payments'));
    }
    //This method just tells u that there is a change in pesapal for your transaction..
    //u need to now query status..retrieve the change...CANCELLED? CONFIRMED?
    public function paymentconfirmation(Request $request)
    {
        $trackingid = $request->input('pesapal_transaction_tracking_id');
        $merchant_reference = $request->input('pesapal_merchant_reference');
        $pesapal_notification_type= $request->input('pesapal_notification_type');

        //use the above to retrieve payment status now..
        $this->checkpaymentstatus($trackingid,$merchant_reference,$pesapal_notification_type);
    }
    //Confirm status of transaction and update the DB
    public function checkpaymentstatus($trackingid,$merchant_reference,$pesapal_notification_type){

        return [
            'trackingId' => $trackingid,
            'merchant_reference' => $merchant_reference,
            'pesapal_notification_type' => $pesapal_notification_type
        ];

        // $status=Pesapal::getMerchantStatus($merchant_reference);
        // $payments = Payment::where('trackingid',$trackingid)->first();
        // $payments -> status = $status;
        // $payments -> payment_method = "PESAPAL";//use the actual method though...
        // $payments -> save();
        // return "success";
    }
}